import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { NewsService } from '../../news.service';
import { Categories, Country } from '../../value';

@Component({
	selector: 'app-news',
	templateUrl: './home.component.html',
	styleUrls: [ './home.component.css' ]
})
export class HomeComponent implements OnInit {
	data: any;
	selectedArticle: any;
	selectedArticleDetail: any;
	category: any;
	country: any;
	sources: any;

	constructor(private newsService: NewsService, private router: Router) {
		// untuk mendefinisikan model Category sebagai variabel category dan ditampilkan sebagai list kategori
		this.category = Categories;
		// untuk mendefinisikan model Country sebagai variabel country dan ditampilkan sebagai list country
		this.country = Country;
		// untuk mendapatkan daftar sumaber berita
		this.newsService.sourcedata('sources').subscribe((data) => {
			this.sources = data;
		});
	}

	ngOnInit() {
		// untuk memanggil semua data berita sesuai top-headlines dengan sumber berita techcrunch
		this.newsService.data('top-headlines?sources=techcrunch').subscribe((data) => {
			this.data = data;
		});
	}

	// fungsi untuk melihat detail berita
	NewsDetail(article) {
		this.newsService.currentArticle = article;
		this.router.navigate([ '/news-detail' ]);
	}
}
