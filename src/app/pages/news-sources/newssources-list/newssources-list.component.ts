import { NewsService } from 'src/app/news.service';

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
	selector: 'app-newssources-list',
	templateUrl: './newssources-list.component.html',
	styleUrls: [ './newssources-list.component.css' ]
})
export class NewssourcesListComponent implements OnInit {
	data: any;
	constructor(private newsService: NewsService, private router: Router) {
		this.newsService.sourcedata('sources').subscribe((data) => {
			this.data = data;
		});
	}

	ngOnInit() {}

	// untuk mendapatkan data berita yang dipilih
	NewsOne(item) {
		this.newsService.data('top-headlines?sources=' + item.id).subscribe((data) => {
			this.newsService.currentArticle = data;
		});
	}
}
