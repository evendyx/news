import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NewsService } from '../../news.service';

@Component({
	selector: 'app-news-sources',
	templateUrl: './news-sources.component.html',
	styleUrls: [ './news-sources.component.css' ]
})
export class NewssourcesComponent implements OnInit {
	data: any;
	selectedArticle: any;
	constructor(private newsService: NewsService, private router: Router, private _Avroute: ActivatedRoute) {}

	ngOnInit() {
		let id = '';
		if (this._Avroute.snapshot.paramMap.get('source')) {
			id = this._Avroute.snapshot.paramMap.get('source');
		}

		// untuk memberikan nilai default dari daftar berita yang ditampilkan
		if (id === null || id === undefined || id === '') {
			this.newsService.sourcedata('sources').subscribe((data) => {
				this.data = data;
				this.newsService.data('top-headlines?sources=' + this.data['sources'][0].id).subscribe((data2) => {
					this.selectedArticle = data2;
				});
			});
		} else {
			this.newsService.sourcedata('sources').subscribe((data) => {
				this.data = data;
			});
		}
	}

	// untuk mendpastkan data berita yang dipilih
	async NewsList(item) {
		this.newsService.data('top-headlines?sources=' + item.id).subscribe((data) => {
			this.selectedArticle = data;
		});
	}
}
