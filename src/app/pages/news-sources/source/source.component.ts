import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NewsService } from '../../../news.service';

@Component({
	selector: 'app-source',
	templateUrl: './source.component.html',
	styleUrls: [ './source.component.css' ]
})
export class SourceComponent implements OnInit {
	data: any;
	selectedArticle: any;
	constructor(private newsService: NewsService, private router: Router, private _Avroute: ActivatedRoute) {}

	ngOnInit() {
		let id = '';
		if (this._Avroute.snapshot.paramMap.get('source')) {
			id = this._Avroute.snapshot.paramMap.get('source');
		}

		// untuk mendapatkan semua data berita dari sumber yang dipilih
		this.newsService.data('top-headlines?sources=' + id).subscribe((data) => {
			console.log(data);
			this.selectedArticle = data;
		});
	}
}
