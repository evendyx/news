import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NewsService } from '../../../news.service';
import { Country } from '../../../value';

@Component({
	selector: 'app-country',
	templateUrl: './country.component.html',
	styleUrls: [ './country.component.css' ]
})
export class CountryComponent implements OnInit {
	data: any;
	country: any;
	selectedArticle: any;

	constructor(private newsService: NewsService, private router: Router, private _Avroute: ActivatedRoute) {
		this.country = Country;
	}

	ngOnInit() {
		let id = '';
		if (this._Avroute.snapshot.paramMap.get('country')) {
			id = this._Avroute.snapshot.paramMap.get('country');
		}

		// untuk mendapatkan semua data berita dari negara yang dipilih
		this.newsService.data('top-headlines?country=' + id).subscribe((data) => {
			this.selectedArticle = data;
		});
	}
}
