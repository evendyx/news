import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NewsService } from '../../news.service';
import { Country } from '../../value';

@Component({
	selector: 'app-news-country',
	templateUrl: './news-country.component.html',
	styleUrls: [ './news-country.component.css' ]
})
export class NewsCountryComponent implements OnInit {
	data: any;
	country: any;
	selectedArticle: any;

	constructor(private newsService: NewsService, private router: Router, private _Avroute: ActivatedRoute) {
		this.country = Country;
	}

	ngOnInit() {
		let id = '';
		if (this._Avroute.snapshot.paramMap.get('country')) {
			id = this._Avroute.snapshot.paramMap.get('country');
		}

		// untuk memberikan nilai default dari daftar berita
		if (id === null || id === undefined || id === '') {
			id = this.country[0]['id'];
		}

		// untuk mendpatakan semua data berita
		this.newsService.data('top-headlines?country=' + id).subscribe((data) => {
			this.selectedArticle = data;
		});
	}

	// untuk mendapatkan data berita dari negara yang dipilih
	NewsOne(item) {
		this.newsService.data('top-headlines?country=' + item.id).subscribe((data) => {
			this.selectedArticle = data;
		});
	}
}
