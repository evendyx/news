import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NewsService } from '../../../news.service';
import { Categories } from '../../../value';

@Component({
	selector: 'app-category',
	templateUrl: './category.component.html',
	styleUrls: [ './category.component.css' ]
})
export class CategoryComponent implements OnInit {
	data: any;
	category: any;
	selectedArticle: any;

	constructor(private newsService: NewsService, private router: Router, private _Avroute: ActivatedRoute) {
		this.category = Categories;
	}

	ngOnInit() {
		let id = '';
		if (this._Avroute.snapshot.paramMap.get('category')) {
			id = this._Avroute.snapshot.paramMap.get('category');
		}

		// untuk mendapatkan semua data berita dari kategori yang dipilih
		this.newsService.data('top-headlines?category=' + id).subscribe((data) => {
			this.selectedArticle = data;
		});
	}
}
