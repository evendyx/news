import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NewsService } from '../../news.service';
import { Categories } from '../../value';

@Component({
  selector: "app-news-category",
  templateUrl: "./news-category.component.html",
  styleUrls: ["./news-category.component.css"],
})
export class NewsCategoryComponent implements OnInit {
  data: any;
  category: any;
  selectedArticle: any;

  constructor(
    private newsService: NewsService,
    private router: Router,
    private _Avroute: ActivatedRoute
  ) {
    this.category = Categories;
    console.log(Categories);
  }

  ngOnInit() {
    let id = "";
    if (this._Avroute.snapshot.paramMap.get("category")) {
      id = this._Avroute.snapshot.paramMap.get("category");
    }

    // to add default category for first view
    console.log(id);
    if (id === null || id === undefined || id === "") {
      console.log(this.category[0]);
      id = this.category[0]["id"];
    }

    // for get all data news
    this.newsService.data("top-headlines?category=" + id).subscribe((data) => {
      console.log(data);
      this.selectedArticle = data;
    });
  }

  // method for category selected
  NewsList(item) {
    this.newsService
      .data("top-headlines?category=" + item.id)
      .subscribe((data) => {
        console.log(data);
        this.selectedArticle = data;
      });
  }
}
