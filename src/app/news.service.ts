import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../environments/environment';
import { Observable, of } from 'rxjs';
const API_URL = 'https://newsapi.org/v2';
const API_KEY = '9db9d14f73de4c34a4a5184c6f685d52';

@Injectable({
	providedIn: 'root'
})
export class NewsService {
	currentArticle: any;
	constructor(private http: HttpClient) {}

	// digunakan untuk pemangilan data berita
	data(url) {
		return this.http.get(API_URL + '/' + url + '&apiKey=' + API_KEY);
	}

	// digunakan untuk pemangilan data berita khusus untuk mendapatkan sumber berita saja
	sourcedata(url) {
		return this.http.get(API_URL + '/' + url + '?apiKey=' + API_KEY);
	}
}
