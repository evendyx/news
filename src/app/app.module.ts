import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './component/navbar/navbar.component';
import { NewsListComponent } from './component/news-list/news-list.component';
import { NewsService } from './news.service';
import { AboutComponent } from './pages/about/about.component';
import { HomeComponent } from './pages/home/home.component';
import { CategoryComponent } from './pages/news-category/category/category.component';
import { NewsCategoryComponent } from './pages/news-category/news-category.component';
import { CountryComponent } from './pages/news-country/country/country.component';
import { NewsCountryComponent } from './pages/news-country/news-country.component';
import { NewsDetailsComponent } from './pages/news-details/news-details.component';
import { NewssourcesComponent } from './pages/news-sources/news-sources.component';
import { NewssourcesListComponent } from './pages/news-sources/newssources-list/newssources-list.component';
import { SourceComponent } from './pages/news-sources/source/source.component';
import { NotFoundComponent } from './pages/not-found/not-found.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NewsDetailsComponent,
    NewsCategoryComponent,
    NewsCountryComponent,
    NewssourcesComponent,
    NewsListComponent,
    NewssourcesListComponent,
    NavbarComponent,
    AboutComponent,
    NotFoundComponent,
    CategoryComponent,
    CountryComponent,
    SourceComponent,
  ],
  imports: [BrowserModule, HttpClientModule, AppRoutingModule],
  providers: [NewsService],
  bootstrap: [AppComponent],
})
export class AppModule {}
