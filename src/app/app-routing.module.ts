import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AboutComponent } from './pages/about/about.component';
import { HomeComponent } from './pages/home/home.component';
import { CategoryComponent } from './pages/news-category/category/category.component';
import { NewsCategoryComponent } from './pages/news-category/news-category.component';
import { CountryComponent } from './pages/news-country/country/country.component';
import { NewsCountryComponent } from './pages/news-country/news-country.component';
import { NewsDetailsComponent } from './pages/news-details/news-details.component';
import { NewssourcesComponent } from './pages/news-sources/news-sources.component';
import { NewssourcesListComponent } from './pages/news-sources/newssources-list/newssources-list.component';
import { SourceComponent } from './pages/news-sources/source/source.component';
import { NotFoundComponent } from './pages/not-found/not-found.component';

const routes: Routes = [
	// aplikasi akan otomatis di redirect (diteruskan) ke halaman home
	{ path: '', redirectTo: '/home', pathMatch: 'full' },
	// halaman awal aplikasi berisi daftar berita, daftar kategori, daftar sumber berita dan daftar negara
	{ path: 'home', component: HomeComponent },
	// halaman berisi tentang web ini (data dummy berupa teks lorem ipsum)
	{ path: 'about', component: AboutComponent },
	// halaman detail dari berita
	{ path: 'news-detail', component: NewsDetailsComponent },
	// halaman yang menyajikan daftar kategori berita
	{ path: 'news-category', component: NewsCategoryComponent },
	// halaman yang menyajikan daftar negara asal berita
	{ path: 'news-country', component: NewsCountryComponent },
	// halaman yang menyajikan daftar sumber berita
	{ path: 'news-sources', component: NewssourcesComponent },
	// halaman yang menyajikan daftar sumber berita versi full (lebih banyak)
	{ path: 'news-sources-list', component: NewssourcesListComponent },
	// halaman dinamis dengan menyesuikan sumber berita (:source) yang dipilih
	{ path: 'news-sources/:source', component: SourceComponent },
	// halaman dinamis dengan menyesuikan kategori berita (:category) yang dipilih
	{ path: 'news-category/:category', component: CategoryComponent },
	// halaman dinamis dengan menyesuikan negara asal berita (:country) yang dipilih
	{ path: 'news-country/:country', component: CountryComponent },
	// halaman eror 404 (halaman yang tidak bisa diakses/tidak ada di aplikasi akan di teruskan ke halaman ini)
	{ path: '**', component: NotFoundComponent }
];

@NgModule({
	imports: [ RouterModule.forRoot(routes) ],
	exports: [ RouterModule ]
})
export class AppRoutingModule {}
