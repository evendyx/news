import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NewsService } from '../../news.service';

@Component({
  selector: "app-news-list",
  templateUrl: "./news-list.component.html",
  styleUrls: ["./news-list.component.css"],
})
export class NewsListComponent implements OnInit {
  @Input() article: any;
  constructor(
    private newsService: NewsService,
    private router: Router,
    private _Avroute: ActivatedRoute
  ) {}

  ngOnInit() {
    // to call method getNews()
    this.getNews();
  }

  // method detail
  NewsDetail(article) {
    this.newsService.currentArticle = article;
    console.log(this.newsService.currentArticle);
    this.router.navigate(["/news-detail"]);
  }

  // for get all data news
  getNews(): void {
    const id = this._Avroute.snapshot.paramMap.get("source");
    if (id !== null && id !== undefined && id !== "") {
      this.newsService.data("top-headlines?sources=" + id).subscribe((data) => {
        console.log(data);
        this.article = data;
      });
    }
  }
}
