export const Categories: any[] = [
	{ id: 'entertainment', name: 'Entertainment' },
	{ id: 'health', name: 'Health' },
	{ id: 'science', name: 'Science' },
	{ id: 'sports', name: 'Sports' },
	{ id: 'technology', name: 'Technology' },
	{ id: 'business', name: 'Business' }
];

export const Country: any[] = [
	{ id: 'au', name: 'Australia' },
	{ id: 'id', name: 'Indonesia' },
	{ id: 'de', name: 'German' },
	{ id: 'fr', name: 'French' },
	{ id: 'gb', name: 'United Kingdom' },
	{ id: 'it', name: 'Italian' },
	{ id: 'in', name: 'India' },
	{ id: 'jp', name: 'Japan' },
	{ id: 'ru', name: 'Russian' },
	{ id: 'us', name: 'United States' }
];
